import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {GateChange} from '../../model/GateChange';
import {map} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class GateChangeService {

  constructor(private http: HttpClient, @Inject('BASE_CONFIG') private config) { }

  searchGateChanges(searchTerm: string): Observable<GateChange[]> {
    const uri = `${this.config.backUrl}/gate-changes/${searchTerm}`;
    return this.http.get<GateChange[]>(uri);
  }
}

