import { TestBed } from '@angular/core/testing';

import { GateChangeService } from './gate-change.service';

describe('GateChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GateChangeService = TestBed.get(GateChangeService);
    expect(service).toBeTruthy();
  });
});
