import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs/index';
import {Arrival} from '../../model/Arrival';
import {Departure} from '../../model/Departure';
import {map} from 'rxjs/internal/operators';
import {GateChange} from '../../model/GateChange';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private http: HttpClient, @Inject('BASE_CONFIG') private config) {
  }

  pairFlightsToGateChange(gcs: GateChange[]): Observable<GateChange[]> {
    const pairArrivals$ = this.http.get(`${this.config.backUrl}/arrivals`).pipe(map((arrivals: Arrival[]) => {
      return gcs.map((gc: GateChange) => {
        gc.arrival = arrivals.find((arrival: Arrival) => arrival.flightNumber === gc.flightNumber);
        return gc;
      });
    }));
    const pairDepartures$ = this.http.get(`${this.config.backUrl}/departures`).pipe(map((departures: Departure[]) => {
      return gcs.map((gc: GateChange) => {
        gc.departure = departures.find((departure: Departure) => departure.flightNumber === gc.flightNumber);
        return gc;
      });
    }));
    return forkJoin(pairArrivals$, pairDepartures$).pipe(map(result => result[0]));
  }
}
