import {Pipe, PipeTransform} from '@angular/core';
import {GateChange} from './model/GateChange';

@Pipe({
  name: 'datePipe'
})
export class DatePipePipe implements PipeTransform {

  transform(gcs: GateChange[], args?: any): GateChange[] {
    if (gcs) {
      gcs.sort((a, b) => {
        let dateA: Date;
        let dateB: Date;
        if (a.arrival) {
          dateA = new Date(a.arrival.arrivalTime);
        } else {
          dateA = new Date(a.departure.departureTime);
        }
        if (b.arrival) {
          dateB = new Date(b.arrival.arrivalTime);
        } else {
          dateB = new Date(b.departure.departureTime);
        }
        return dateA.getTime() - dateB.getTime();
      });
      return gcs;
    } else {
      return gcs;
    }
  }

}
