import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { DatePipePipe } from './date-pipe.pipe';
import {NoCacheInterceptor} from "./no-cache.interceptor";

@NgModule({
    declarations: [AppComponent, DatePipePipe],
    imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
    providers: [
      {
        provide: 'BASE_CONFIG',
        useValue: {
          backUrl: 'http://95.217.57.184:3000',
          localBackUrl: 'http://127.0.0.1:3000'
        }
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: NoCacheInterceptor,
        multi: true
      }
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
