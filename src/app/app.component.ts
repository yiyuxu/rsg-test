import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {
  debounceTime, distinctUntilChanged, filter, map, switchMap,
  take
} from 'rxjs/internal/operators';
import {GateChangeService} from './service/gate-change/gate-change.service';
import {FlightService} from './service/flight/flight.service';
import {Observable} from 'rxjs/index';
import {GateChange} from './model/GateChange';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  form: FormGroup;
  gateChange$: Observable<GateChange[]>;
  gateChangeScenarioTwo$: Observable<GateChange[]>;

  constructor(private fb: FormBuilder,
              private gateChangeS$: GateChangeService,
              private flightS$: FlightService) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      flightSearch: ['']
    });
    this.initInputDetection();
  }

  clearInput(ev: KeyboardEvent) {
    if (ev.keyCode === 8) {
      this.gateChange$ = null;
      this.gateChangeScenarioTwo$ = null;
    } else {
      this.initInputDetection();
    }
  }

  private initInputDetection() {
    this.gateChange$ = this.form.get('flightSearch').valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(s => s && s.length > 1),
        switchMap(searchTerms => this.gateChangeS$.searchGateChanges(searchTerms)),
        switchMap((gcs: GateChange[]) => this.flightS$.pairFlightsToGateChange(gcs)),
        map((gatechanges: GateChange[]) => gatechanges.slice(0, 5)),
        take(1)
      );
    this.gateChangeScenarioTwo$ = this.form.get('flightSearch').valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(s => s && s.length > 1),
        switchMap(searchTerms => this.gateChangeS$.searchGateChanges(searchTerms)),
        switchMap((gcs: GateChange[]) => this.flightS$.pairFlightsToGateChange(gcs)),
        take(1)
      );
  }
}

