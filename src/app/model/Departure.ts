export interface Departure {
  flightNumber: string;
  gate: string;
  destination: string;
  departureTime: string;
  passengers: number;
}
