export interface Arrival {
  flightNumber: string;
  gate: string;
  origin: string;
  arrivalTime: string;
  passengers: number;
}
