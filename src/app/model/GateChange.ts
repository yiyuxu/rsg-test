import {Arrival} from './Arrival';
import {Departure} from './Departure';

export interface GateChange {
  currentGate: string;
  previousGate: string;
  flightNumber: string;
  direction: string;
  arrival?: Arrival;
  departure?: Departure;
}
