import { removeOppositeChars } from './challenge-2';

describe('removeOppositeChars', () => {
  const input = 'VvbBfpPFrRyRrNpYyPDlLdVvNnMmnOCcosOoSoOfkKKkFJjyYjJWwHhnSstuBbdsSDqQUqQkKVvILlVvGgjJiVcCvvfBbvVoOGgFn';
  it('result should be', () => {
    expect(removeOppositeChars(input)).toBe('yntvn');
  });
});
