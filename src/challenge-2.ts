/**
 * You start with a string consisting of uppercase and lowercase letters (example: dabAcCaCBAcCcaDA.)
 * We want you to write a function that takes a string and removes all characters that are followed by the same character of the
 * opposite case. The rules are:
 *
 * For input: "aA", a and A are of the opposite case, returning an empty string.
 * For input: "abBA", bB are of the opposite case, leaving aA. As above, this is also removed, returning an empty string as well.
 * For input: "abAB", no two adjacent characters are of the same type, so the same string is returned.
 * For input: "aabAAB", even though aa and AA are the same character, their cases match, and so nothing happens.
 * Now, consider a larger example, dabAcCaCBAcCcaDA:
 *
 *   - dabAcCaCBAcCcaDA  The first 'cC' is removed.
 *   - dabAaCBAcCcaDA    This creates 'Aa', which is removed.
 *   - dabCBAcCcaDA      Either 'cC' or 'Cc' are removed (the result is the same).
 *   - dabCBAcaDA        No further actions can be taken.
 *
 * What is the solution for: 'VvbBfpPFrRyRrNpYyPDlLdVvNnMmnOCcosOoSoOfkKKkFJjyYjJWwHhnSstuBbdsSDqQUqQkKVvILlVvGgjJiVcCvvfBbvVoOGgFn'?
 */

export const removeOppositeChars = (input: string): string => {
  const result = []; // Output result
  for (let i = 0; i < input.length; i++) {
    const len = result.length; // Get length of output array.
    const lastEleOfArray = result[len - 1]; // Get the last element of output array.
    // init the array using the first item of string
    if (i === 0
      || lastEleOfArray === undefined
      || lastEleOfArray === null) {
      result.push(input[i]);
    }
    if (i !== 0) {
      // If the current item of string is the opposite case of the last item of the output array
      // Then delete the last item of the object
      if (lastEleOfArray) {
        if (input[i].charCodeAt(0) !== lastEleOfArray.charCodeAt(0)
          && input[i].toLowerCase() === lastEleOfArray.toLowerCase()) {
          result.splice(len - 1, 1);
        } else {
          // else push the item to the output array
          result.push(input[i]);
        }
      }
    }
  }
  // convert array to string
  return result.join('');
};
