/**
 * Find the closest number to num from the listOfNumbers. Assume the input array only contains unique values
 */
export const getClosestValue = (num: number, listOfNumbers: number[]): number => {
  return listOfNumbers.reduce(
    (previousValue, currentValue) =>
      Math.abs(currentValue - num) < Math.abs(previousValue - num)
        ? currentValue
        : previousValue
  );
};
